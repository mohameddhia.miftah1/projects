import { Component, OnDestroy, OnInit } from '@angular/core';
import {interval, Subscription} from 'rxjs';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit , OnDestroy{

    title = 'Device Application';

	secondCounter : number = 0;
    counterSubscription: Subscription = new Subscription;
	constructor() { }

	ngOnInit() {
        /* 1 st method : to infinity problem */
		/*const counter = interval(1000);
        counter.subscribe(
            (value :number)=> { this.secondCounter = value;},
            (error : any) => { console.log("une erreur a ete rencontrée !")},
            ()=> {console.log('observable completee !')},
        );*/

        /* 2nd method : solve infinity problem */
        const counter = interval(1000);
        this.counterSubscription = counter.subscribe((value: number) => this.secondCounter = value)
	}

    ngOnDestroy(){
        this.counterSubscription.unsubscribe()
    }
}
