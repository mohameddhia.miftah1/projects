import {Injectable} from '@angular/core';
import {User} from "../models/User.model";
import {Subject} from "rxjs";

@Injectable({
    providedIn: 'root'
})
export class UserService {
    private userList: User[] = [
        {
            firstName : 'Dhia',
            lastName:'Miftah',
            email: "mohameddhia.miftah@gmail.com",
            drinkPreference:'Thé',
            hobbies:[ 'coder','les nouveaux Tech']
        }
    ];
    userSubject = new Subject<User[]>();

    constructor() {
    }

    emitUsers() {
        this.userSubject.next(this.userList.slice());
    }

    addUser(user: User) {
        this.userList.push(user);
        this.emitUsers();
    }

}
