import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';


@Component({
    selector: 'app-authentication',
    templateUrl: './authentication.component.html',
    styleUrls: ['./authentication.component.scss']
})
export class AuthenticationComponent implements OnInit {

    isAuthenticated: boolean = false;
    constructor(private authService: AuthenticationService, private router: Router) { }

    ngOnInit(): void {
        this.isAuthenticated = this.authService.isAuth;
    }

    onSignIn() {
        this.authService.signIn().then(
            () => {
                this.isAuthenticated = this.authService.isAuth;
                this.router.navigate(['devices']);
            }
        )
    }

    onSignOut() {
        this.authService.signOut();
        this.isAuthenticated = this.authService.isAuth;
    }

}
