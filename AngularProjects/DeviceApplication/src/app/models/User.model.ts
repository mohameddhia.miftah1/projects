export class User {

    // private _firstName: string = "";
    // private _lastName: string = "";

    constructor(public firstName: string,
                public lastName: string,
                public email: string,
                public drinkPreference: string,
                public hobbies?: string[],
    ) {

    }

    // get firstName(): string {
    //     return this._firstName;
    // }
    //
    // set firstName(value: string) {
    //     this._firstName = value;
    // }
    //
    // get lastName(): string {
    //     return this._lastName;
    // }
    //
    // set lastName(value: string) {
    //     this._lastName = value;
    // }

}
