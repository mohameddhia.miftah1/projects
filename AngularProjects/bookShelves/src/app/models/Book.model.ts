export class BookModel {

    photo: string | undefined;

    constructor(public title: string, public author: string) {
    }
}
