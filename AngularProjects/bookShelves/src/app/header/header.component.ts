import {Component, OnInit} from '@angular/core';
import {AuthService} from "../services/auth.service";
import {getAuth, onAuthStateChanged} from "firebase/auth";

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

    isAuth: boolean = false;

    constructor(private authService: AuthService) {
    }

    ngOnInit(): void {
        // this.isAuth = this.authService.isAuth ;
        // firebase.auth().onAuthStateChanged(
        //     (userCredential) => {
        //         // Signed in
        //         const user = userCredential.user;
        //         if (user) {
        //             this.isAuth = true;
        //         } else {
        //             this.isAuth = false;
        //         }
        //     }
        // )

        const auth = getAuth();
        onAuthStateChanged(auth, (user) => {
            if (user) {
                this.isAuth = true;
                const uid = user.uid;
            } else {
                this.isAuth = false;
            }
        });
    }

    onSignOut() {
        this.authService.signOutUser();
    }
}
