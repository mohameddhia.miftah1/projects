import { Component, OnInit } from '@angular/core';
import { MedSpecialiste } from 'app/modal/med-specialiste';
import { SpecialisteService } from 'app/service/specialiste-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ajout-med-spec',
  templateUrl: './ajout-med-spec.component.html',
  styleUrls: ['./ajout-med-spec.component.scss']
})
export class AjoutMedSpecComponent implements OnInit {

  private medecin:MedSpecialiste;

  constructor(private patientService:SpecialisteService, private router:Router) { }
 ngOnInit() {
 this.medecin=this.patientService.getter();
 }

 saveForm(){
  this.patientService.saveMedecinSpec(this.medecin).subscribe((medecin)=>{
    this.router.navigate(['/tableMed/MedSpecialiste']);
   },(error)=>{console.log(error);});
  }
}
