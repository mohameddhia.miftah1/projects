import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditMedSpecComponent } from './edit-med-spec.component';

describe('EditMedSpecComponent', () => {
  let component: EditMedSpecComponent;
  let fixture: ComponentFixture<EditMedSpecComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditMedSpecComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditMedSpecComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
