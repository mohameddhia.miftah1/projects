import { Component, OnInit } from '@angular/core';
import { MedecinService } from 'app/service/medecin-service';
import { Medecin } from 'app/modal/medecin';
import { Router } from '@angular/router';//pour la redirection

@Component({
  selector: 'app-basic-table',
  templateUrl: './basic-table.component.html',
  styleUrls: ['./basic-table.component.scss']
})
export class BasicTableComponent implements OnInit {

  medecins: Array<any>;
  medecinsF:Medecin[];
  medecin:Medecin;


  constructor(private medecinService: MedecinService, private router:Router) { }

  ngOnInit() {
    this.medecinService.getAllMedecin().subscribe(data => {
      this.medecins = data;
    },(error)=>{console.log(error);
  });
}

deleteMedecin(medecin){
  this.medecinService.deleteMedecin(medecin.id).subscribe((data)=>{
    this.medecinsF.splice(this.medecinsF.indexOf(medecin),1);
  });
  location.reload();
}

getAllSpec()
{
  this.router.navigate(['/tableMed/MedSpecialiste']);

}

getAllGen()
{
  this.router.navigate(['/tableMed/MedGeneraliste']);
}
  
}
