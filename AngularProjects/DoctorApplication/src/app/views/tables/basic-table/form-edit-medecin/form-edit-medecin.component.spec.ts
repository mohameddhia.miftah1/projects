import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormEditMedecinComponent } from './form-edit-medecin.component';

describe('FormEditMedecinComponent', () => {
  let component: FormEditMedecinComponent;
  let fixture: ComponentFixture<FormEditMedecinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormEditMedecinComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormEditMedecinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
