import { Component, OnInit } from '@angular/core';
import { Medecin } from 'app/modal/medecin';
import { MedecinService } from 'app/service/medecin-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-form-edit-medecin',
  templateUrl: './form-edit-medecin.component.html',
  styleUrls: ['./form-edit-medecin.component.scss']
})
export class FormEditMedecinComponent implements OnInit {

  private medecin:Medecin;
  constructor(private medecinService:MedecinService, private router:Router) { }
  ngOnInit() {
  this.medecin=this.medecinService.getter();
  }


} 
