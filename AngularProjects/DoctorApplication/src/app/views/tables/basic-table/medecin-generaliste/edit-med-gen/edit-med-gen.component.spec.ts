import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditMedGenComponent } from './edit-med-gen.component';

describe('EditMedGenComponent', () => {
  let component: EditMedGenComponent;
  let fixture: ComponentFixture<EditMedGenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditMedGenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditMedGenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
