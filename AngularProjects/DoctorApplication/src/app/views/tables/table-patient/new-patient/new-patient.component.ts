import { Component, OnInit } from '@angular/core';
import { Patient } from 'app/modal/patient';
import { PatientService } from 'app/service/patient-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-patient',
  templateUrl: './new-patient.component.html',
  styleUrls: ['./new-patient.component.scss']
})
export class NewPatientComponent implements OnInit {
  private patient:Patient;

  constructor(private patientService:PatientService, private router:Router) { }
 ngOnInit() {
 this.patient=this.patientService.getter();
 }

 saveForm2(){
  this.patientService.savePatient(this.patient).subscribe((patient)=>{
    this.router.navigate(['/tables/table-patient']);
   },(error)=>{console.log(error);});
  }   
}
