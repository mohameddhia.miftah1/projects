import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
// importer le map de rxjs
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { Medecin } from 'app/modal/medecin';


@Injectable()
export class Recherche {

  private baseUrl:string='http://localhost:8081/cabinet';
  private headers= new Headers({'Content-Type':'application/json'});
//  private options = new RequestOptions({headers:this.headers});

  constructor(private http: HttpClient) {
  }

  //Recherche pour médecin
  findMedByNom(nom:string): Observable<any> {
    return this.http.get(this.baseUrl+'/findMedecinByLastName/'+nom);
  }

  //Recherche pour Patient
  findPatByNom(nom:string): Observable<any> {
    return this.http.get(this.baseUrl+'/findPatientByLastName/'+nom);
  }

    //afficher en cas d'erreur
  errorHandle(error:Response){
    return Observable.throw(error||"Server Error");
  }
}
