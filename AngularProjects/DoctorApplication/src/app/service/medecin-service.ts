import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Medecin } from 'app/modal/medecin';
// importer le map de rxjs
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class MedecinService {

  private medecin:Medecin;
  private baseUrl:string='http://localhost:8081/cabinet';
  private headers= new Headers({'Content-Type':'application/json'});
//  private options = new RequestOptions({headers:this.headers});

  constructor(private http: HttpClient) {
  }


  
  getAllMedecin(): Observable<any> {
    return this.http.get(this.baseUrl+'/getAllMedecin');
  }

  deleteMedecin(id:number){ 
    return this.http.delete(this.baseUrl+'/deleteMedecin/'+id)
    .map((response:Response)=>response.json()) 
    .catch(this.errorHandle);   
  } 
        
    //afficher en cas d'erreur
  errorHandle(error:Response){
    return Observable.throw(error||"Server Error");
  }
    //getters & setters pour l'edit
  setter(medecin:Medecin){
    this.medecin=medecin;
  }
  getter(){
    return this.medecin;
  }   
}