import { Patient } from "app/modal/patient";
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs/Observable";
import { Injectable } from "@angular/core";
import { Response, Headers, RequestOptions} from '@angular/http';

@Injectable()
export class PatientService {

  private patient:Patient;
  private baseUrl:string='http://localhost:8081/cabinet';
  private headers= new Headers({'Content-Type':'application/json;charset=utf-8','Accept':'application/json,text/plain,*/*'});
//  private options = new RequestOptions({headers:this.headers});

  constructor(private http: HttpClient) {  }

  getAllPatients(): Observable<any> {
    return this.http.get(this.baseUrl+'/getAllPatient');
  }

  savePatient(patient:Patient):Observable<any>{
    return this.http.post(this.baseUrl+'/addPatient',patient);
  }
                 
  updatePatient(id:number,patient:Patient): Observable<any>{
    return this.http.put(this.baseUrl+'/updatePatient/'+id,patient);      
  }
        
  deletePatient(id:number){  
    return this.http.delete(this.baseUrl+'/deletePatient/'+id)//,this.options)
    .map((response:Response)=>response.json()) 
    .catch(this.errorHandle);   
  } 
          
      //afficher en cas d'erreur
  errorHandle(error:Response){
    return Observable.throw(error||"Server Error");
  }

  //getters & setters pour l'edit
  setter(patient:Patient){
    this.patient=patient;
  }
  getter(){
    return this.patient;
  }
}
