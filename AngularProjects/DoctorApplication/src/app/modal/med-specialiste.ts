export class MedSpecialiste {

    id : number ;
    nom: string;
    prenom: string;
    codePublic : string;
    dateNaissance: Date;
    email: string;
    photo : string;
    specialite : string;
    
    constructor(){}
}
