import { Patient } from "./patient";
import {Medecin} from './medecin';

export class Consultation {
    id : number ;
    date: Date;
    notes: string;
    medecin: Medecin;
    patient:Patient;
    
    constructor(){
    }
}
