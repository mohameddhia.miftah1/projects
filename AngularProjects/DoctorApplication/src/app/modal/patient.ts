import { MedGeneraliste } from "./med-generaliste";

export class Patient {

    id : number ;
    nom: string;
    prenom: string;
    dateNaissance: Date;
    email: string;
    photo : string;
    tel: string;
    generaliste : MedGeneraliste;


    constructor(){
    }
}
