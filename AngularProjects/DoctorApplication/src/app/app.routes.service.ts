
import { Map1Component } from './views/maps/map1/map1.component';
import { ModalsComponent } from './views/modals/modals.component';
import { BasicTableComponent } from './views/tables/basic-table/basic-table.component';
import { Profile1Component } from './views/profile/profile1/profile1.component';
import { RouterModule, Route } from '@angular/router';
import { ModuleWithProviders} from '@angular/core';
//import { Component } from '@angular/core';
import { NotFoundComponent } from './views/errors/not-found/not-found.component';
import { Dashboard1Component } from './views/dashboards/dashboard1/dashboard1.component';
import { TableConsultationComponent } from './views/tables/table-consultation/table-consultation.component';
import { TablePatientComponent } from './views/tables/table-patient/table-patient.component';
//import { Medecin } from './modal/medecin';
//import { FormEditMedecinComponent } from './views/tables/basic-table/form-edit-medecin/form-edit-medecin.component';
import { EditFormPatientComponent } from './views/tables/table-patient/edit-form-patient/edit-form-patient.component';
import { NewPatientComponent } from './views/tables/table-patient/new-patient/new-patient.component';
import { MedecinGeneralisteComponent } from './views/tables/basic-table/medecin-generaliste/medecin-generaliste.component';
import { MedecinSpecialisteComponent } from './views/tables/basic-table/medecin-specialiste/medecin-specialiste.component';
import { EditMedSpecComponent } from './views/tables/basic-table/medecin-specialiste/edit-med-spec/edit-med-spec.component';
import { AjoutMedSpecComponent } from './views/tables/basic-table/medecin-specialiste/ajout-med-spec/ajout-med-spec.component';
import { EditMedGenComponent } from './views/tables/basic-table/medecin-generaliste/edit-med-gen/edit-med-gen.component';
import { AjoutMedGenComponent } from './views/tables/basic-table/medecin-generaliste/ajout-med-gen/ajout-med-gen.component';
import { EditConsultationComponent } from './views/tables/table-consultation/edit-consultation/edit-consultation.component';
import { AjoutConsultationComponent } from './views/tables/table-consultation/ajout-consultation/ajout-consultation.component';


const routes: Route[] = [
  { path: '', pathMatch: 'full', redirectTo: 'dashboards/v1' },
  { path: 'dashboards', children:
    [
      { path: 'v1', component: Dashboard1Component },
    ]
  },
  { path: 'profiles', children:[
      { path: 'profile1', component: Profile1Component },
    ]
  },
  { path: 'tables', children:[
      { path: 'table-medecin', component: BasicTableComponent},
      { path: 'table-patient', component: TablePatientComponent },
      { path: 'table-consultation', component: TableConsultationComponent },
    ]
  },
  { path:'tableMed',children:[
      {path:'MedGeneraliste',component:MedecinGeneralisteComponent},
      {path:'MedSpecialiste',component:MedecinSpecialisteComponent}
    ]
  },
  { path:'MedSpec',children:[
    {path:'modifierMedSpec',component:EditMedSpecComponent},
    {path:'ajouterMedSpec',component:AjoutMedSpecComponent}
  ]
  },  
  { path:'MedGen',children:[
    {path:'modifierMedGen',component:EditMedGenComponent},
    {path:'ajouterMedGen',component:AjoutMedGenComponent}
  ]
  },
  { path:'tablePat',children:[
    {path:'modifierPat',component:EditFormPatientComponent},
    {path:'ajoutPat',component:NewPatientComponent}
  ]
  },
  { path:'tableCons',children:[
    {path:'modifierCons',component:EditConsultationComponent},
    {path:'ajoutCons',component:AjoutConsultationComponent}
  ]
  },
  { path: 'maps', children:[
      { path: 'map1', component: Map1Component},
    ]
  },
  { path: 'modals', component: ModalsComponent},
  { path: '**', component: NotFoundComponent },

];

export const AppRoutes: ModuleWithProviders = RouterModule.forRoot(routes);
