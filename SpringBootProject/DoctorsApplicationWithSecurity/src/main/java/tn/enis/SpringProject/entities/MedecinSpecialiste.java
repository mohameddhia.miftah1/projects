package tn.enis.SpringProject.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;

@Entity
@DiscriminatorValue("MS")
public class MedecinSpecialiste extends Medecin implements Serializable {

	private static final long serialVersionUID = 1L;

	private String specialite;

	@ManyToMany(cascade = CascadeType.ALL)
	private Collection<Patient> patients;

	public MedecinSpecialiste() {
		super();
	}

	public MedecinSpecialiste(String nom, String prenom, Date dateNaissance, String email, byte[] photo,
			String codePublic, String specialite) {
		super(nom, prenom, dateNaissance, email, photo, codePublic);
		this.specialite = specialite;
	}

	public String getSpecialite() {
		return specialite;
	}

	public void setSpecialite(String specialite) {
		this.specialite = specialite;
	}

	public Collection<Patient> getPatients() {
		return patients;
	}

	public void setPatients(Collection<Patient> patients) {
		this.patients = patients;
	}
}
