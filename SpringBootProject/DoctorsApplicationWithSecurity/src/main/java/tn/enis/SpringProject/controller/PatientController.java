package tn.enis.SpringProject.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tn.enis.SpringProject.entities.Patient;
import tn.enis.SpringProject.service.PatientService;

@RestController
@RequestMapping("/cabinet")
@CrossOrigin("*")
public class PatientController {

	@Autowired
	PatientService patientService;

	@GetMapping("/getAllPatient")
	@Secured({"ROLE_ADMIN"})
	public List<Patient> getAllPatient() {
		return patientService.getAllPatient();
	}

	@GetMapping(value = "/getOnePatient/{id}")
	@Secured({"ROLE_ADMIN"})
	public Patient getOnePatient(@PathVariable Long id) {
		return patientService.getOnePatient(id);
	}

	@GetMapping("/findPatientByLastName/{nom}")
	@Secured({"ROLE_ADMIN"})
	public List<Patient> findPatientByLastName(@PathVariable String nom) {
		return patientService.findPatientByLastName(nom);
	}

	@GetMapping("/findPatientByName/{prenom}")
	@Secured({"ROLE_ADMIN"})
	public List<Patient> findPatientByName(@PathVariable String prenom) {
		return patientService.findPatientByName(prenom);
	}

	@GetMapping("/sortPatientByLastName")
	@Secured({"ROLE_ADMIN"})
	public List<Patient> sortPatientByLastName() {
		return patientService.sortByLastName();
	}

	@GetMapping("/sortPatientByName")
	@Secured({"ROLE_ADMIN"})
	public List<Patient> sortPatientByName() {
		return patientService.sortByName();
	}

	@PostMapping("/addPatient")
	public void addPatient(@RequestBody Patient patient) {
		patientService.addPatient(patient);
	}

	@PutMapping(value = "/updatePatient/{id}")
	public void UpdatePatient(@PathVariable Long id, @RequestBody Patient patient) {
		patientService.updatePatient(id, patient);
	}

	@DeleteMapping("/deletePatient/{id}")
	public void deletePatient(@PathVariable Long id) {
		patientService.deletePatient(id);
	}
}
