package tn.enis.SpringProject.service;

import java.util.List;

import tn.enis.SpringProject.entities.Medecin;

public interface IMedecinService {

	public Medecin getOneMedecin(Long id);

	public List<Medecin> getAllMedecin();

	public String deleteMedecin(Long id);

	public List<Medecin> findMedecinByLastName(String nom);

	public List<Medecin> findMedecinByName(String prenom);

	public List<Medecin> sortByLastName();

	public List<Medecin> sortByName();
}
