package tn.com.Employee.employeeManager;

public class MyDerivedClass {
	private MyDerivedClass() {};

	private static class AHolder {
		private static final MyDerivedClass INSTANCE = new MyDerivedClass();
	}

	private static MyDerivedClass getInstance() {
		return AHolder.INSTANCE;
	}

}
