//
// Ce fichier a �t� g�n�r� par l'impl�mentation de r�f�rence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apport�e � ce fichier sera perdue lors de la recompilation du sch�ma source. 
// G�n�r� le : 2019.04.12 � 09:51:52 AM WAT 
//


package tn.talan.pfe.XmlDto.install;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour ReadingTypeList complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="ReadingTypeList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="readingTypeId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *           &lt;element name="ReadingType" type="{http://www.emeter.com/energyip/amiinterface}ReadingType"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReadingTypeList", propOrder = {
    "readingTypeId",
    "readingType"
})
public class ReadingTypeList {

    protected String readingTypeId;
    @XmlElement(name = "ReadingType")
    protected ReadingType readingType;

    /**
     * Obtient la valeur de la propri�t� readingTypeId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReadingTypeId() {
        return readingTypeId;
    }

    /**
     * D�finit la valeur de la propri�t� readingTypeId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReadingTypeId(String value) {
        this.readingTypeId = value;
    }

    /**
     * Obtient la valeur de la propri�t� readingType.
     * 
     * @return
     *     possible object is
     *     {@link ReadingType }
     *     
     */
    public ReadingType getReadingType() {
        return readingType;
    }

    /**
     * D�finit la valeur de la propri�t� readingType.
     * 
     * @param value
     *     allowed object is
     *     {@link ReadingType }
     *     
     */
    public void setReadingType(ReadingType value) {
        this.readingType = value;
    }

}
