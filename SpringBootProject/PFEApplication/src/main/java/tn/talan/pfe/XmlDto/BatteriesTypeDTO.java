package tn.talan.pfe.XmlDto;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BatteriesType", propOrder = { "battery" })
public class BatteriesTypeDTO {

	@XmlElement(name = "Battery", required = true)
	protected List<BatteryTypeDTO> battery;

	/**
	 * Gets the value of the battery property.
	 * 
	 * <p>
	 * This accessor method returns a reference to the live list, not a snapshot.
	 * Therefore any modification you make to the returned list will be present
	 * inside the JAXB object. This is why there is not a <CODE>set</CODE> method
	 * for the battery property.
	 * 
	 * <p>
	 * For example, to add a new item, do as follows:
	 * 
	 * <pre>
	 * getBattery().add(newItem);
	 * </pre>
	 * 
	 * 
	 * <p>
	 * Objects of the following type(s) are allowed in the list {@link BatteryTypeDTO }
	 * 
	 * 
	 */
	public List<BatteryTypeDTO> getBattery() {
		if (battery == null) {
			battery = new ArrayList<BatteryTypeDTO>();
		}
		return this.battery;
	}

}
