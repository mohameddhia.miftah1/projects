//
// Ce fichier a �t� g�n�r� par l'impl�mentation de r�f�rence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apport�e � ce fichier sera perdue lors de la recompilation du sch�ma source. 
// G�n�r� le : 2019.04.12 � 09:51:52 AM WAT 
//


package tn.talan.pfe.XmlDto.install;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour ConfigurationProfileStatus complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="ConfigurationProfileStatus">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="expectedValues" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="clearValue" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="isValid" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="remarks" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConfigurationProfileStatus")
public class ConfigurationProfileStatus {

    @XmlAttribute(name = "expectedValues")
    protected String expectedValues;
    @XmlAttribute(name = "clearValue")
    protected Boolean clearValue;
    @XmlAttribute(name = "isValid")
    protected Boolean isValid;
    @XmlAttribute(name = "remarks")
    protected String remarks;

    /**
     * Obtient la valeur de la propri�t� expectedValues.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpectedValues() {
        return expectedValues;
    }

    /**
     * D�finit la valeur de la propri�t� expectedValues.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpectedValues(String value) {
        this.expectedValues = value;
    }

    /**
     * Obtient la valeur de la propri�t� clearValue.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isClearValue() {
        return clearValue;
    }

    /**
     * D�finit la valeur de la propri�t� clearValue.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setClearValue(Boolean value) {
        this.clearValue = value;
    }

    /**
     * Obtient la valeur de la propri�t� isValid.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsValid() {
        return isValid;
    }

    /**
     * D�finit la valeur de la propri�t� isValid.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsValid(Boolean value) {
        this.isValid = value;
    }

    /**
     * Obtient la valeur de la propri�t� remarks.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * D�finit la valeur de la propri�t� remarks.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemarks(String value) {
        this.remarks = value;
    }

}
