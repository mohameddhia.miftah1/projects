//
// Ce fichier a �t� g�n�r� par l'impl�mentation de r�f�rence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apport�e � ce fichier sera perdue lors de la recompilation du sch�ma source. 
// G�n�r� le : 2019.04.12 � 09:51:52 AM WAT 
//


package tn.talan.pfe.XmlDto.install;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java pour CollectionGroup complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="CollectionGroup">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="readfrequency" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="readwindowstarttime" type="{http://www.w3.org/2001/XMLSchema}time"/>
 *         &lt;element name="readwindowendtime" type="{http://www.w3.org/2001/XMLSchema}time"/>
 *         &lt;element name="deliveryfrequency" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="deliverywindowstarttime" type="{http://www.w3.org/2001/XMLSchema}time"/>
 *         &lt;element name="deliverywindowendtime" type="{http://www.w3.org/2001/XMLSchema}time"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CollectionGroup", propOrder = {
    "name",
    "readfrequency",
    "readwindowstarttime",
    "readwindowendtime",
    "deliveryfrequency",
    "deliverywindowstarttime",
    "deliverywindowendtime"
})
public class CollectionGroup {

    @XmlElement(required = true)
    protected String name;
    @XmlElement(required = true)
    protected String readfrequency;
    @XmlElement(required = true)
    @XmlSchemaType(name = "time")
    protected XMLGregorianCalendar readwindowstarttime;
    @XmlElement(required = true)
    @XmlSchemaType(name = "time")
    protected XMLGregorianCalendar readwindowendtime;
    @XmlElement(required = true)
    protected String deliveryfrequency;
    @XmlElement(required = true)
    @XmlSchemaType(name = "time")
    protected XMLGregorianCalendar deliverywindowstarttime;
    @XmlElement(required = true)
    @XmlSchemaType(name = "time")
    protected XMLGregorianCalendar deliverywindowendtime;

    /**
     * Obtient la valeur de la propri�t� name.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * D�finit la valeur de la propri�t� name.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Obtient la valeur de la propri�t� readfrequency.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReadfrequency() {
        return readfrequency;
    }

    /**
     * D�finit la valeur de la propri�t� readfrequency.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReadfrequency(String value) {
        this.readfrequency = value;
    }

    /**
     * Obtient la valeur de la propri�t� readwindowstarttime.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getReadwindowstarttime() {
        return readwindowstarttime;
    }

    /**
     * D�finit la valeur de la propri�t� readwindowstarttime.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setReadwindowstarttime(XMLGregorianCalendar value) {
        this.readwindowstarttime = value;
    }

    /**
     * Obtient la valeur de la propri�t� readwindowendtime.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getReadwindowendtime() {
        return readwindowendtime;
    }

    /**
     * D�finit la valeur de la propri�t� readwindowendtime.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setReadwindowendtime(XMLGregorianCalendar value) {
        this.readwindowendtime = value;
    }

    /**
     * Obtient la valeur de la propri�t� deliveryfrequency.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeliveryfrequency() {
        return deliveryfrequency;
    }

    /**
     * D�finit la valeur de la propri�t� deliveryfrequency.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeliveryfrequency(String value) {
        this.deliveryfrequency = value;
    }

    /**
     * Obtient la valeur de la propri�t� deliverywindowstarttime.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDeliverywindowstarttime() {
        return deliverywindowstarttime;
    }

    /**
     * D�finit la valeur de la propri�t� deliverywindowstarttime.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDeliverywindowstarttime(XMLGregorianCalendar value) {
        this.deliverywindowstarttime = value;
    }

    /**
     * Obtient la valeur de la propri�t� deliverywindowendtime.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDeliverywindowendtime() {
        return deliverywindowendtime;
    }

    /**
     * D�finit la valeur de la propri�t� deliverywindowendtime.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDeliverywindowendtime(XMLGregorianCalendar value) {
        this.deliverywindowendtime = value;
    }

}
