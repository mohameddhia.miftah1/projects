package tn.talan.pfe.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import tn.talan.pfe.entities.Mbus;

public interface MbusRepository extends JpaRepository<Mbus, String> {

}
