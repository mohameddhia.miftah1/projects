package tn.talan.pfe.service.Imp;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.XMLGregorianCalendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import tn.talan.pfe.XmlDto.discovery.DevdiscDTO;
import tn.talan.pfe.common.ConvertionClass;
import tn.talan.pfe.dao.MeterRepository;
import tn.talan.pfe.entities.Meter;
import tn.talan.pfe.service.IActivationRequestService;

@Service
@Component
public class DiscoveryReportService {

	@Autowired
	MeterRepository meterRepository;
	@Autowired
	IActivationRequestService iActivationRequestService;

	/* les classes a utilisé */
	ConvertionClass convertionClass = new ConvertionClass();

	@JmsListener(destination = "DiscoveryReport.queue")
	public void receive(String discoveryReport) throws Exception {

		String message = "";

		StringReader discoveryReportFile = new StringReader(discoveryReport);

		JAXBContext context = JAXBContext.newInstance(DevdiscDTO.class);
		Unmarshaller unmarshaller = context.createUnmarshaller();
		DevdiscDTO devdiscDTO = (DevdiscDTO) unmarshaller.unmarshal(discoveryReportFile);

		String devId = devdiscDTO.getDevId();
		Meter meter = meterRepository.findById(devId).get();

		if (meter == null) {
			message = "Meter does not exist !";
			System.out.println(message);
		} else {
			message = "Meter found !";
			System.out.println(message);

			XMLGregorianCalendar xmlDate = devdiscDTO.getTime();
			System.out.println(convertionClass.convertxmlGregorianCalendartoDate(xmlDate));

			meter.setDiscoveryDateTime(convertionClass.convertxmlGregorianCalendartoDate(xmlDate));
//			System.out.println(meter.getDiscoveryDateTime());

			String lifeCycleStatus = meter.getLifeCycleStatus();
			System.out.println(lifeCycleStatus);
			if (lifeCycleStatus.equals("PROVISIONED")) {
//				meter.setLifeCycleStatus("DISCOVERED");
				System.out.println("from PROVISIONED to DISCOVERED");
				/* save changes */
//				meterRepository.saveAndFlush(meter);

			} else if (lifeCycleStatus.equals("INSTALLED")) {
//				meter.setLifeCycleStatus("RECONCILED");
				System.out.println("from INSTALLED to DISCOVERED");

				/* save changes */
//				meterRepository.saveAndFlush(meter);

				/* activate Meter */
				iActivationRequestService.activateMeter(devId);
			}

			/* save changes */
//			meterRepository.saveAndFlush(meter);

		}
	}
}
