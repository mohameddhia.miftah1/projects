//
// Ce fichier a �t� g�n�r� par l'impl�mentation de r�f�rence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apport�e � ce fichier sera perdue lors de la recompilation du sch�ma source. 
// G�n�r� le : 2019.04.12 � 09:51:52 AM WAT 
//


package tn.talan.pfe.XmlDto.install;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java pour anonymous complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="header" type="{http://www.emeter.com/energyip/amiinterface}MessageHeader"/>
 *         &lt;element name="reply" type="{http://www.emeter.com/energyip/amiinterface}ReplyHeader" minOccurs="0"/>
 *         &lt;element name="meterReads" type="{http://www.emeter.com/energyip/amiinterface}MeterReadings" minOccurs="0"/>
 *         &lt;element name="activationDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="expectInitialReads" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="expectFinalReads" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="initialMeterReads" type="{http://www.emeter.com/energyip/amiinterface}MeterReading" minOccurs="0"/>
 *         &lt;element name="finalMeterReads" type="{http://www.emeter.com/energyip/amiinterface}MeterReading" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "header",
    "reply",
    "meterReads",
    "activationDateTime",
    "expectInitialReads",
    "expectFinalReads",
    "initialMeterReads",
    "finalMeterReads"
})
@XmlRootElement(name = "MeterAssetUpdateReplyMessage")
public class MeterAssetUpdateReplyMessage {

    @XmlElement(required = true)
    protected MessageHeader header;
    protected ReplyHeader reply;
    protected MeterReadings meterReads;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar activationDateTime;
    protected Boolean expectInitialReads;
    protected Boolean expectFinalReads;
    protected MeterReading initialMeterReads;
    protected MeterReading finalMeterReads;

    /**
     * Obtient la valeur de la propri�t� header.
     * 
     * @return
     *     possible object is
     *     {@link MessageHeader }
     *     
     */
    public MessageHeader getHeader() {
        return header;
    }

    /**
     * D�finit la valeur de la propri�t� header.
     * 
     * @param value
     *     allowed object is
     *     {@link MessageHeader }
     *     
     */
    public void setHeader(MessageHeader value) {
        this.header = value;
    }

    /**
     * Obtient la valeur de la propri�t� reply.
     * 
     * @return
     *     possible object is
     *     {@link ReplyHeader }
     *     
     */
    public ReplyHeader getReply() {
        return reply;
    }

    /**
     * D�finit la valeur de la propri�t� reply.
     * 
     * @param value
     *     allowed object is
     *     {@link ReplyHeader }
     *     
     */
    public void setReply(ReplyHeader value) {
        this.reply = value;
    }

    /**
     * Obtient la valeur de la propri�t� meterReads.
     * 
     * @return
     *     possible object is
     *     {@link MeterReadings }
     *     
     */
    public MeterReadings getMeterReads() {
        return meterReads;
    }

    /**
     * D�finit la valeur de la propri�t� meterReads.
     * 
     * @param value
     *     allowed object is
     *     {@link MeterReadings }
     *     
     */
    public void setMeterReads(MeterReadings value) {
        this.meterReads = value;
    }

    /**
     * Obtient la valeur de la propri�t� activationDateTime.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getActivationDateTime() {
        return activationDateTime;
    }

    /**
     * D�finit la valeur de la propri�t� activationDateTime.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setActivationDateTime(XMLGregorianCalendar value) {
        this.activationDateTime = value;
    }

    /**
     * Obtient la valeur de la propri�t� expectInitialReads.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isExpectInitialReads() {
        return expectInitialReads;
    }

    /**
     * D�finit la valeur de la propri�t� expectInitialReads.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExpectInitialReads(Boolean value) {
        this.expectInitialReads = value;
    }

    /**
     * Obtient la valeur de la propri�t� expectFinalReads.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isExpectFinalReads() {
        return expectFinalReads;
    }

    /**
     * D�finit la valeur de la propri�t� expectFinalReads.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExpectFinalReads(Boolean value) {
        this.expectFinalReads = value;
    }

    /**
     * Obtient la valeur de la propri�t� initialMeterReads.
     * 
     * @return
     *     possible object is
     *     {@link MeterReading }
     *     
     */
    public MeterReading getInitialMeterReads() {
        return initialMeterReads;
    }

    /**
     * D�finit la valeur de la propri�t� initialMeterReads.
     * 
     * @param value
     *     allowed object is
     *     {@link MeterReading }
     *     
     */
    public void setInitialMeterReads(MeterReading value) {
        this.initialMeterReads = value;
    }

    /**
     * Obtient la valeur de la propri�t� finalMeterReads.
     * 
     * @return
     *     possible object is
     *     {@link MeterReading }
     *     
     */
    public MeterReading getFinalMeterReads() {
        return finalMeterReads;
    }

    /**
     * D�finit la valeur de la propri�t� finalMeterReads.
     * 
     * @param value
     *     allowed object is
     *     {@link MeterReading }
     *     
     */
    public void setFinalMeterReads(MeterReading value) {
        this.finalMeterReads = value;
    }

}
