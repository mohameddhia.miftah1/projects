package tn.talan.pfe.config;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnectionFactory;

public class ConnectActiveMq {

//	@SendTo("ActivationRequest")
	public void sendActivationRequest(String activationRequest) throws Exception {

		ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://localhost:61616");
		Connection connection = connectionFactory.createConnection();

		Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		connection.start();

		Destination d = session.createQueue("ActivationRequest.queue");
		MessageProducer messageProducer = session.createProducer(d);
//		producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
		TextMessage textMessage = session.createTextMessage();
		textMessage.setText(activationRequest);
		messageProducer.send(textMessage);
		System.out.println(activationRequest);
		System.out.println("donnnnneee");
	}
}
