package tn.talan.pfe.XmlDto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import tn.talan.xmlenc_.EncryptedKeyType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "header", "body" })
@XmlRootElement(name = "ShipmentFile")
public class ShipmentFileDTO {

	@XmlElement(name = "Header", required = true)
	protected ShipmentFileDTO.Header header;
	@XmlElement(name = "Body", required = true)
	protected ShipmentFileDTO.Body body;

	/**
	 * @return possible object is {@link ShipmentFileDTO.Header }
	 */
	public ShipmentFileDTO.Header getHeader() {
		return header;
	}

	/**
	 * @param value allowed object is {@link ShipmentFileDTO.Header }
	 */
	public void setHeader(ShipmentFileDTO.Header value) {
		this.header = value;
	}

	/**
	 * @return possible object is {@link ShipmentFileDTO.Body }
	 */
	public ShipmentFileDTO.Body getBody() {
		return body;
	}

	/**
	 * @param value allowed object is {@link ShipmentFileDTO.Body }
	 */
	public void setBody(ShipmentFileDTO.Body value) {
		this.body = value;
	}

	/*****************************************************************************************
	 **************************** class Body *************************************************
	 *****************************************************************************************/

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "Body", propOrder = { "shipment" })
	public static class Body {

		@XmlElement(name = "Shipment", required = true)
		protected ShipmentFileDTO.Body.Shipment shipment;

		/**
		 * @return possible object is {@link ShipmentFileDTO.Body.Shipment }
		 */
		public ShipmentFileDTO.Body.Shipment getShipment() {
			return shipment;
		}

		/**
		 * @param value allowed object is {@link ShipmentFileDTO.Body.Shipment }
		 */
		public void setShipment(ShipmentFileDTO.Body.Shipment value) {
			this.shipment = value;
		}

		/*****************************************************************************************
		 **************************** class Shipment *********************************************
		 *****************************************************************************************/

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "Shipment", propOrder = { "main", "header", "pallet" })
		public static class Shipment {

			@XmlElement(name = "Main", required = true)
			protected MainTypeDTO main;
			@XmlElement(name = "Header", required = true)
			protected HeaderTypeDTO header;
			@XmlElement(name = "Pallet", required = true)
			protected List<PalletTypeDTO> pallet;

			/**
			 * @return possible object is {@link MainTypeDTO }
			 */
			public MainTypeDTO getMain() {
				return main;
			}

			/**
			 * @param value allowed object is {@link MainTypeDTO }
			 */
			public void setMain(MainTypeDTO value) {
				this.main = value;
			}

			/**
			 * @return possible object is {@link HeaderTypeDTO }
			 */
			public HeaderTypeDTO getHeader() {
				return header;
			}

			/**
			 * @param value allowed object is {@link HeaderTypeDTO }
			 */
			public void setHeader(HeaderTypeDTO value) {
				this.header = value;
			}

			/**
			 * Gets the value of the pallet property.
			 * 
			 * <p>
			 * This accessor method returns a reference to the live list, not a snapshot.
			 * Therefore any modification you make to the returned list will be present
			 * inside the JAXB object. This is why there is not a <CODE>set</CODE> method
			 * for the pallet property.
			 * 
			 * <p>
			 * For example, to add a new item, do as follows:
			 * 
			 * <pre>
			 * getPallet().add(newItem);
			 * </pre>
			 * 
			 * <p>
			 * Objects of the following type(s) are allowed in the list {@link PalletTypeDTO }
			 * 
			 */
			public List<PalletTypeDTO> getPallet() {
				if (pallet == null) {
					pallet = new ArrayList<PalletTypeDTO>();
				}
				return this.pallet;
			}

		}

	}

	/*****************************************************************************************
	 ****************************** class Header *********************************************
	 *****************************************************************************************/

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "security" })
	public static class Header {

		@XmlElement(name = "Security", required = true)
		protected ShipmentFileDTO.Header.Security security;

		/**
		 * @return possible object is {@link ShipmentFileDTO.Header.Security }
		 */
		public ShipmentFileDTO.Header.Security getSecurity() {
			return security;
		}

		/**
		 * @param value allowed object is {@link ShipmentFileDTO.Header.Security }
		 */
		public void setSecurity(ShipmentFileDTO.Header.Security value) {
			this.security = value;
		}

		/*****************************************************************************************
		 ****************************** class Security *******************************************
		 *****************************************************************************************/

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = { "securityKeySiconia" })
		public static class Security {

			@XmlElement(name = "SecurityKey_Siconia", required = true)
			protected ShipmentFileDTO.Header.Security.SecurityKeySiconia securityKeySiconia;

			/**
			 * @return possible object is
			 *         {@link ShipmentFileDTO.Header.Security.SecurityKeySiconia }
			 */
			public ShipmentFileDTO.Header.Security.SecurityKeySiconia getSecurityKeySiconia() {
				return securityKeySiconia;
			}

			/**
			 * @param value allowed object is
			 *              {@link ShipmentFileDTO.Header.Security.SecurityKeySiconia }
			 */
			public void setSecurityKeySiconia(ShipmentFileDTO.Header.Security.SecurityKeySiconia value) {
				this.securityKeySiconia = value;
			}

			/*****************************************************************************************
			 **************************** class SecurityKeySiconia ***********************************
			 *****************************************************************************************/

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = { "encryptedKey" })
			public static class SecurityKeySiconia {

				@XmlElement(name = "EncryptedKey", namespace = "http://www.w3.org/2001/04/xmlenc#", required = true)
				protected EncryptedKeyType encryptedKey;

				/**
				 * @return possible object is {@link EncryptedKeyType }
				 */
				public EncryptedKeyType getEncryptedKey() {
					return encryptedKey;
				}

				/**
				 * @param value allowed object is {@link EncryptedKeyType }
				 */
				public void setEncryptedKey(EncryptedKeyType value) {
					this.encryptedKey = value;
				}
			}
		}
	}
}
