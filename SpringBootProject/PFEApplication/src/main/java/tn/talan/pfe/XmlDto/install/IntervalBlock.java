//
// Ce fichier a �t� g�n�r� par l'impl�mentation de r�f�rence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apport�e � ce fichier sera perdue lors de la recompilation du sch�ma source. 
// G�n�r� le : 2019.04.12 � 09:51:52 AM WAT 
//


package tn.talan.pfe.XmlDto.install;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour IntervalBlock complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="IntervalBlock">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="readingTypeId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReadingType" type="{http://www.emeter.com/energyip/amiinterface}ReadingType" minOccurs="0"/>
 *         &lt;element name="IReading" type="{http://www.emeter.com/energyip/amiinterface}IReading" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IntervalBlock", propOrder = {
    "readingTypeId",
    "readingType",
    "iReading"
})
public class IntervalBlock {

    protected String readingTypeId;
    @XmlElement(name = "ReadingType")
    protected ReadingType readingType;
    @XmlElement(name = "IReading")
    protected List<IReading> iReading;

    /**
     * Obtient la valeur de la propri�t� readingTypeId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReadingTypeId() {
        return readingTypeId;
    }

    /**
     * D�finit la valeur de la propri�t� readingTypeId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReadingTypeId(String value) {
        this.readingTypeId = value;
    }

    /**
     * Obtient la valeur de la propri�t� readingType.
     * 
     * @return
     *     possible object is
     *     {@link ReadingType }
     *     
     */
    public ReadingType getReadingType() {
        return readingType;
    }

    /**
     * D�finit la valeur de la propri�t� readingType.
     * 
     * @param value
     *     allowed object is
     *     {@link ReadingType }
     *     
     */
    public void setReadingType(ReadingType value) {
        this.readingType = value;
    }

    /**
     * Gets the value of the iReading property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the iReading property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIReading().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IReading }
     * 
     * 
     */
    public List<IReading> getIReading() {
        if (iReading == null) {
            iReading = new ArrayList<IReading>();
        }
        return this.iReading;
    }

}
