//
// Ce fichier a �t� g�n�r� par l'impl�mentation de r�f�rence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apport�e � ce fichier sera perdue lors de la recompilation du sch�ma source. 
// G�n�r� le : 2019.04.12 � 09:51:52 AM WAT 
//


package tn.talan.pfe.XmlDto.install;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java pour CheckLoadLimitReplyHeader complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="CheckLoadLimitReplyHeader">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.emeter.com/energyip/amiinterface}ReplyHeader">
 *       &lt;sequence>
 *         &lt;element name="verificationDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="isVerified" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="hasloadLimitProfile" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="loadLimitProfile" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CheckLoadLimitReplyHeader", propOrder = {
    "verificationDateTime",
    "isVerified",
    "hasloadLimitProfile",
    "loadLimitProfile"
})
public class CheckLoadLimitReplyHeader
    extends ReplyHeader
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar verificationDateTime;
    protected boolean isVerified;
    protected boolean hasloadLimitProfile;
    @XmlElement(required = true)
    protected String loadLimitProfile;

    /**
     * Obtient la valeur de la propri�t� verificationDateTime.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getVerificationDateTime() {
        return verificationDateTime;
    }

    /**
     * D�finit la valeur de la propri�t� verificationDateTime.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setVerificationDateTime(XMLGregorianCalendar value) {
        this.verificationDateTime = value;
    }

    /**
     * Obtient la valeur de la propri�t� isVerified.
     * 
     */
    public boolean isIsVerified() {
        return isVerified;
    }

    /**
     * D�finit la valeur de la propri�t� isVerified.
     * 
     */
    public void setIsVerified(boolean value) {
        this.isVerified = value;
    }

    /**
     * Obtient la valeur de la propri�t� hasloadLimitProfile.
     * 
     */
    public boolean isHasloadLimitProfile() {
        return hasloadLimitProfile;
    }

    /**
     * D�finit la valeur de la propri�t� hasloadLimitProfile.
     * 
     */
    public void setHasloadLimitProfile(boolean value) {
        this.hasloadLimitProfile = value;
    }

    /**
     * Obtient la valeur de la propri�t� loadLimitProfile.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoadLimitProfile() {
        return loadLimitProfile;
    }

    /**
     * D�finit la valeur de la propri�t� loadLimitProfile.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoadLimitProfile(String value) {
        this.loadLimitProfile = value;
    }

}
