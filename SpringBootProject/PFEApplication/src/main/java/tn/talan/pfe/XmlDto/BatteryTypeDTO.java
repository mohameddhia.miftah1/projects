package tn.talan.pfe.XmlDto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BatteryType", propOrder = { "startOfBatteryLifeDate", "batteryType", "batteryReplacementDate" })
public class BatteryTypeDTO {

	@XmlElement(name = "Start_of_battery_life_date", required = true)
	protected String startOfBatteryLifeDate;
	@XmlElement(name = "Battery_type", required = true)
	protected String batteryType;
	@XmlElement(name = "Battery_replacement_date", required = true)
	protected String batteryReplacementDate;

	/**
	 * @return possible object is {@link String }
	 */
	public String getStartOfBatteryLifeDate() {
		return startOfBatteryLifeDate;
	}

	/**
	 * @param value allowed object is {@link String }
	 */
	public void setStartOfBatteryLifeDate(String value) {
		this.startOfBatteryLifeDate = value;
	}

	/**
	 * @return possible object is {@link String }
	 */
	public String getBatteryType() {
		return batteryType;
	}

	/**
	 * @param value allowed object is {@link String }
	 */
	public void setBatteryType(String value) {
		this.batteryType = value;
	}

	/**
	 * @return possible object is {@link String }
	 */
	public String getBatteryReplacementDate() {
		return batteryReplacementDate;
	}

	/**
	 * @param value allowed object is {@link String }
	 */
	public void setBatteryReplacementDate(String value) {
		this.batteryReplacementDate = value;
	}
}