//
// Ce fichier a �t� g�n�r� par l'impl�mentation de r�f�rence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apport�e � ce fichier sera perdue lors de la recompilation du sch�ma source. 
// G�n�r� le : 2019.04.12 � 09:51:52 AM WAT 
//


package tn.talan.pfe.XmlDto.install;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour ComFunction complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="ComFunction">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.emeter.com/energyip/amiinterface}Asset">
 *       &lt;sequence>
 *         &lt;element name="amrAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="amrSystem" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ComFunction", propOrder = {
    "amrAddress",
    "amrSystem"
})
public class ComFunction
    extends Asset
{

    protected String amrAddress;
    protected String amrSystem;

    /**
     * Obtient la valeur de la propri�t� amrAddress.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmrAddress() {
        return amrAddress;
    }

    /**
     * D�finit la valeur de la propri�t� amrAddress.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmrAddress(String value) {
        this.amrAddress = value;
    }

    /**
     * Obtient la valeur de la propri�t� amrSystem.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmrSystem() {
        return amrSystem;
    }

    /**
     * D�finit la valeur de la propri�t� amrSystem.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmrSystem(String value) {
        this.amrSystem = value;
    }

}
