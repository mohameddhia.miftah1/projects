//
// Ce fichier a �t� g�n�r� par l'impl�mentation de r�f�rence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apport�e � ce fichier sera perdue lors de la recompilation du sch�ma source. 
// G�n�r� le : 2019.04.12 � 09:51:52 AM WAT 
//


package tn.talan.pfe.XmlDto.install;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java pour MeterSendTextMessageRequestPayload complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="MeterSendTextMessageRequestPayload">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="meterAsset" type="{http://www.emeter.com/energyip/amiinterface}MeterAsset"/>
 *         &lt;element name="requestPriority" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="startDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="duration" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="textMessage" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="messagePriority" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="confirmationRequired" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="issuerId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="issuerTrackingId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="transmissionMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="reason" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MeterSendTextMessageRequestPayload", propOrder = {
    "meterAsset",
    "requestPriority",
    "startDateTime",
    "duration",
    "textMessage",
    "messagePriority",
    "confirmationRequired",
    "issuerId",
    "issuerTrackingId",
    "transmissionMode",
    "reason"
})
public class MeterSendTextMessageRequestPayload {

    @XmlElement(required = true)
    protected MeterAsset meterAsset;
    protected String requestPriority;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar startDateTime;
    @XmlElement(required = true)
    protected BigInteger duration;
    @XmlElement(required = true)
    protected String textMessage;
    @XmlElement(required = true)
    protected String messagePriority;
    protected boolean confirmationRequired;
    @XmlElement(required = true)
    protected String issuerId;
    @XmlElement(required = true)
    protected String issuerTrackingId;
    @XmlElement(required = true)
    protected String transmissionMode;
    @XmlElement(required = true)
    protected String reason;

    /**
     * Obtient la valeur de la propri�t� meterAsset.
     * 
     * @return
     *     possible object is
     *     {@link MeterAsset }
     *     
     */
    public MeterAsset getMeterAsset() {
        return meterAsset;
    }

    /**
     * D�finit la valeur de la propri�t� meterAsset.
     * 
     * @param value
     *     allowed object is
     *     {@link MeterAsset }
     *     
     */
    public void setMeterAsset(MeterAsset value) {
        this.meterAsset = value;
    }

    /**
     * Obtient la valeur de la propri�t� requestPriority.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestPriority() {
        return requestPriority;
    }

    /**
     * D�finit la valeur de la propri�t� requestPriority.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestPriority(String value) {
        this.requestPriority = value;
    }

    /**
     * Obtient la valeur de la propri�t� startDateTime.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartDateTime() {
        return startDateTime;
    }

    /**
     * D�finit la valeur de la propri�t� startDateTime.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartDateTime(XMLGregorianCalendar value) {
        this.startDateTime = value;
    }

    /**
     * Obtient la valeur de la propri�t� duration.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getDuration() {
        return duration;
    }

    /**
     * D�finit la valeur de la propri�t� duration.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setDuration(BigInteger value) {
        this.duration = value;
    }

    /**
     * Obtient la valeur de la propri�t� textMessage.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTextMessage() {
        return textMessage;
    }

    /**
     * D�finit la valeur de la propri�t� textMessage.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTextMessage(String value) {
        this.textMessage = value;
    }

    /**
     * Obtient la valeur de la propri�t� messagePriority.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessagePriority() {
        return messagePriority;
    }

    /**
     * D�finit la valeur de la propri�t� messagePriority.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessagePriority(String value) {
        this.messagePriority = value;
    }

    /**
     * Obtient la valeur de la propri�t� confirmationRequired.
     * 
     */
    public boolean isConfirmationRequired() {
        return confirmationRequired;
    }

    /**
     * D�finit la valeur de la propri�t� confirmationRequired.
     * 
     */
    public void setConfirmationRequired(boolean value) {
        this.confirmationRequired = value;
    }

    /**
     * Obtient la valeur de la propri�t� issuerId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssuerId() {
        return issuerId;
    }

    /**
     * D�finit la valeur de la propri�t� issuerId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssuerId(String value) {
        this.issuerId = value;
    }

    /**
     * Obtient la valeur de la propri�t� issuerTrackingId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssuerTrackingId() {
        return issuerTrackingId;
    }

    /**
     * D�finit la valeur de la propri�t� issuerTrackingId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssuerTrackingId(String value) {
        this.issuerTrackingId = value;
    }

    /**
     * Obtient la valeur de la propri�t� transmissionMode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransmissionMode() {
        return transmissionMode;
    }

    /**
     * D�finit la valeur de la propri�t� transmissionMode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransmissionMode(String value) {
        this.transmissionMode = value;
    }

    /**
     * Obtient la valeur de la propri�t� reason.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReason() {
        return reason;
    }

    /**
     * D�finit la valeur de la propri�t� reason.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReason(String value) {
        this.reason = value;
    }

}
