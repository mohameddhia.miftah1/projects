package tn.talan.pfe.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import tn.talan.pfe.entities.ShipmentFile;

public interface ShipmentFileRepository extends JpaRepository<ShipmentFile, Long> {

}
