package tn.talan.pfe.XmlDto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import tn.talan.xmldsig_.KeyInfoType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MainType", propOrder = { "company", "supplier", "publicKeySiconia" })
public class MainTypeDTO {

	@XmlElement(name = "Company", required = true)
	protected String company;
	@XmlElement(name = "Supplier", required = true)
	protected String supplier;
	@XmlElement(name = "Public_key_Siconia", required = true)
	protected MainTypeDTO.PublicKeySiconia publicKeySiconia;

	/**
	 * @return possible object is {@link String }
	 */
	public String getCompany() {
		return company;
	}

	/**
	 * @param value allowed object is {@link String }
	 */
	public void setCompany(String value) {
		this.company = value;
	}

	/**
	 * @return possible object is {@link String }
	 */
	public String getSupplier() {
		return supplier;
	}

	/**
	 * @param value allowed object is {@link String }
	 */
	public void setSupplier(String value) {
		this.supplier = value;
	}

	/**
	 * @return possible object is {@link MainTypeDTO.PublicKeySiconia }
	 */
	public MainTypeDTO.PublicKeySiconia getPublicKeySiconia() {
		return publicKeySiconia;
	}

	/**
	 * @param value allowed object is {@link MainTypeDTO.PublicKeySiconia }
	 */
	public void setPublicKeySiconia(MainTypeDTO.PublicKeySiconia value) {
		this.publicKeySiconia = value;
	}

	/*****************************************************************************************
	 **************************** class PublicKeySiconia *************************************
	 *****************************************************************************************/

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = { "keyInfo" })
	public static class PublicKeySiconia {

		@XmlElement(name = "KeyInfo", namespace = "http://www.w3.org/2000/09/xmldsig#", required = true)
		protected KeyInfoType keyInfo;

		/**
		 * @return possible object is {@link KeyInfoType }
		 */
		public KeyInfoType getKeyInfo() {
			return keyInfo;
		}

		/**
		 * @param value allowed object is {@link KeyInfoType }
		 */
		public void setKeyInfo(KeyInfoType value) {
			this.keyInfo = value;
		}
	}
}
