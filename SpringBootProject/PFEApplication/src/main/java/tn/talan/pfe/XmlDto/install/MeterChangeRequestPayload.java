//
// Ce fichier a �t� g�n�r� par l'impl�mentation de r�f�rence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apport�e � ce fichier sera perdue lors de la recompilation du sch�ma source. 
// G�n�r� le : 2019.04.12 � 09:51:52 AM WAT 
//


package tn.talan.pfe.XmlDto.install;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java pour MeterChangeRequestPayload complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="MeterChangeRequestPayload">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="meterAsset" type="{http://www.emeter.com/energyip/amiinterface}MeterAsset"/>
 *         &lt;element name="comFunction" type="{http://www.emeter.com/energyip/amiinterface}ComFunction"/>
 *         &lt;element name="serviceDeliveryPoint" type="{http://www.emeter.com/energyip/amiinterface}ServiceDeliveryPoint"/>
 *         &lt;element name="serviceLocation" type="{http://www.emeter.com/energyip/amiinterface}ServiceLocation"/>
 *         &lt;element name="customerAccount" type="{http://www.emeter.com/energyip/amiinterface}CustomerAccount"/>
 *         &lt;element name="provisioningGroup" type="{http://www.emeter.com/energyip/amiinterface}ProvisioningGroup" minOccurs="0"/>
 *         &lt;element name="collectionGroup" type="{http://www.emeter.com/energyip/amiinterface}CollectionGroup" minOccurs="0"/>
 *         &lt;element name="broadcastGroup" type="{http://www.emeter.com/energyip/amiinterface}BroadcastGroup" minOccurs="0"/>
 *         &lt;element name="requestPriority" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="executeStartTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="executeExpireTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="measurementProfile" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="touProfile" type="{http://www.emeter.com/energyip/amiinterface}ConfigurationProfile" minOccurs="0"/>
 *         &lt;element name="holidayProfile" type="{http://www.emeter.com/energyip/amiinterface}ConfigurationProfile" minOccurs="0"/>
 *         &lt;element name="loadLimitProfile" type="{http://www.emeter.com/energyip/amiinterface}ConfigurationProfile" minOccurs="0"/>
 *         &lt;element name="loadControlTimeSwitchProfile" type="{http://www.emeter.com/energyip/amiinterface}ConfigurationProfile" minOccurs="0"/>
 *         &lt;element name="issuerId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="issuerTrackingId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="reason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="activationDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MeterChangeRequestPayload", propOrder = {
    "meterAsset",
    "comFunction",
    "serviceDeliveryPoint",
    "serviceLocation",
    "customerAccount",
    "provisioningGroup",
    "collectionGroup",
    "broadcastGroup",
    "requestPriority",
    "executeStartTime",
    "executeExpireTime",
    "measurementProfile",
    "touProfile",
    "holidayProfile",
    "loadLimitProfile",
    "loadControlTimeSwitchProfile",
    "issuerId",
    "issuerTrackingId",
    "reason",
    "activationDateTime"
})
@XmlSeeAlso({
    MeterUpdateRequestPayload.class,
    MeterDeleteRequestPayload.class,
    MeterRetireRequestPayload.class,
    MeterAddRequestPayload.class
})
public class MeterChangeRequestPayload {

    @XmlElement(required = true)
    protected MeterAsset meterAsset;
    @XmlElement(required = true)
    protected ComFunction comFunction;
    @XmlElement(required = true)
    protected ServiceDeliveryPoint serviceDeliveryPoint;
    @XmlElement(required = true)
    protected ServiceLocation serviceLocation;
    @XmlElement(required = true)
    protected CustomerAccount customerAccount;
    protected ProvisioningGroup provisioningGroup;
    protected CollectionGroup collectionGroup;
    protected BroadcastGroup broadcastGroup;
    protected String requestPriority;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar executeStartTime;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar executeExpireTime;
    protected String measurementProfile;
    protected ConfigurationProfile touProfile;
    protected ConfigurationProfile holidayProfile;
    protected ConfigurationProfile loadLimitProfile;
    protected ConfigurationProfile loadControlTimeSwitchProfile;
    protected String issuerId;
    protected String issuerTrackingId;
    protected String reason;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar activationDateTime;

    /**
     * Obtient la valeur de la propri�t� meterAsset.
     * 
     * @return
     *     possible object is
     *     {@link MeterAsset }
     *     
     */
    public MeterAsset getMeterAsset() {
        return meterAsset;
    }

    /**
     * D�finit la valeur de la propri�t� meterAsset.
     * 
     * @param value
     *     allowed object is
     *     {@link MeterAsset }
     *     
     */
    public void setMeterAsset(MeterAsset value) {
        this.meterAsset = value;
    }

    /**
     * Obtient la valeur de la propri�t� comFunction.
     * 
     * @return
     *     possible object is
     *     {@link ComFunction }
     *     
     */
    public ComFunction getComFunction() {
        return comFunction;
    }

    /**
     * D�finit la valeur de la propri�t� comFunction.
     * 
     * @param value
     *     allowed object is
     *     {@link ComFunction }
     *     
     */
    public void setComFunction(ComFunction value) {
        this.comFunction = value;
    }

    /**
     * Obtient la valeur de la propri�t� serviceDeliveryPoint.
     * 
     * @return
     *     possible object is
     *     {@link ServiceDeliveryPoint }
     *     
     */
    public ServiceDeliveryPoint getServiceDeliveryPoint() {
        return serviceDeliveryPoint;
    }

    /**
     * D�finit la valeur de la propri�t� serviceDeliveryPoint.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceDeliveryPoint }
     *     
     */
    public void setServiceDeliveryPoint(ServiceDeliveryPoint value) {
        this.serviceDeliveryPoint = value;
    }

    /**
     * Obtient la valeur de la propri�t� serviceLocation.
     * 
     * @return
     *     possible object is
     *     {@link ServiceLocation }
     *     
     */
    public ServiceLocation getServiceLocation() {
        return serviceLocation;
    }

    /**
     * D�finit la valeur de la propri�t� serviceLocation.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceLocation }
     *     
     */
    public void setServiceLocation(ServiceLocation value) {
        this.serviceLocation = value;
    }

    /**
     * Obtient la valeur de la propri�t� customerAccount.
     * 
     * @return
     *     possible object is
     *     {@link CustomerAccount }
     *     
     */
    public CustomerAccount getCustomerAccount() {
        return customerAccount;
    }

    /**
     * D�finit la valeur de la propri�t� customerAccount.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerAccount }
     *     
     */
    public void setCustomerAccount(CustomerAccount value) {
        this.customerAccount = value;
    }

    /**
     * Obtient la valeur de la propri�t� provisioningGroup.
     * 
     * @return
     *     possible object is
     *     {@link ProvisioningGroup }
     *     
     */
    public ProvisioningGroup getProvisioningGroup() {
        return provisioningGroup;
    }

    /**
     * D�finit la valeur de la propri�t� provisioningGroup.
     * 
     * @param value
     *     allowed object is
     *     {@link ProvisioningGroup }
     *     
     */
    public void setProvisioningGroup(ProvisioningGroup value) {
        this.provisioningGroup = value;
    }

    /**
     * Obtient la valeur de la propri�t� collectionGroup.
     * 
     * @return
     *     possible object is
     *     {@link CollectionGroup }
     *     
     */
    public CollectionGroup getCollectionGroup() {
        return collectionGroup;
    }

    /**
     * D�finit la valeur de la propri�t� collectionGroup.
     * 
     * @param value
     *     allowed object is
     *     {@link CollectionGroup }
     *     
     */
    public void setCollectionGroup(CollectionGroup value) {
        this.collectionGroup = value;
    }

    /**
     * Obtient la valeur de la propri�t� broadcastGroup.
     * 
     * @return
     *     possible object is
     *     {@link BroadcastGroup }
     *     
     */
    public BroadcastGroup getBroadcastGroup() {
        return broadcastGroup;
    }

    /**
     * D�finit la valeur de la propri�t� broadcastGroup.
     * 
     * @param value
     *     allowed object is
     *     {@link BroadcastGroup }
     *     
     */
    public void setBroadcastGroup(BroadcastGroup value) {
        this.broadcastGroup = value;
    }

    /**
     * Obtient la valeur de la propri�t� requestPriority.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestPriority() {
        return requestPriority;
    }

    /**
     * D�finit la valeur de la propri�t� requestPriority.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestPriority(String value) {
        this.requestPriority = value;
    }

    /**
     * Obtient la valeur de la propri�t� executeStartTime.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExecuteStartTime() {
        return executeStartTime;
    }

    /**
     * D�finit la valeur de la propri�t� executeStartTime.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExecuteStartTime(XMLGregorianCalendar value) {
        this.executeStartTime = value;
    }

    /**
     * Obtient la valeur de la propri�t� executeExpireTime.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExecuteExpireTime() {
        return executeExpireTime;
    }

    /**
     * D�finit la valeur de la propri�t� executeExpireTime.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExecuteExpireTime(XMLGregorianCalendar value) {
        this.executeExpireTime = value;
    }

    /**
     * Obtient la valeur de la propri�t� measurementProfile.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMeasurementProfile() {
        return measurementProfile;
    }

    /**
     * D�finit la valeur de la propri�t� measurementProfile.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMeasurementProfile(String value) {
        this.measurementProfile = value;
    }

    /**
     * Obtient la valeur de la propri�t� touProfile.
     * 
     * @return
     *     possible object is
     *     {@link ConfigurationProfile }
     *     
     */
    public ConfigurationProfile getTouProfile() {
        return touProfile;
    }

    /**
     * D�finit la valeur de la propri�t� touProfile.
     * 
     * @param value
     *     allowed object is
     *     {@link ConfigurationProfile }
     *     
     */
    public void setTouProfile(ConfigurationProfile value) {
        this.touProfile = value;
    }

    /**
     * Obtient la valeur de la propri�t� holidayProfile.
     * 
     * @return
     *     possible object is
     *     {@link ConfigurationProfile }
     *     
     */
    public ConfigurationProfile getHolidayProfile() {
        return holidayProfile;
    }

    /**
     * D�finit la valeur de la propri�t� holidayProfile.
     * 
     * @param value
     *     allowed object is
     *     {@link ConfigurationProfile }
     *     
     */
    public void setHolidayProfile(ConfigurationProfile value) {
        this.holidayProfile = value;
    }

    /**
     * Obtient la valeur de la propri�t� loadLimitProfile.
     * 
     * @return
     *     possible object is
     *     {@link ConfigurationProfile }
     *     
     */
    public ConfigurationProfile getLoadLimitProfile() {
        return loadLimitProfile;
    }

    /**
     * D�finit la valeur de la propri�t� loadLimitProfile.
     * 
     * @param value
     *     allowed object is
     *     {@link ConfigurationProfile }
     *     
     */
    public void setLoadLimitProfile(ConfigurationProfile value) {
        this.loadLimitProfile = value;
    }

    /**
     * Obtient la valeur de la propri�t� loadControlTimeSwitchProfile.
     * 
     * @return
     *     possible object is
     *     {@link ConfigurationProfile }
     *     
     */
    public ConfigurationProfile getLoadControlTimeSwitchProfile() {
        return loadControlTimeSwitchProfile;
    }

    /**
     * D�finit la valeur de la propri�t� loadControlTimeSwitchProfile.
     * 
     * @param value
     *     allowed object is
     *     {@link ConfigurationProfile }
     *     
     */
    public void setLoadControlTimeSwitchProfile(ConfigurationProfile value) {
        this.loadControlTimeSwitchProfile = value;
    }

    /**
     * Obtient la valeur de la propri�t� issuerId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssuerId() {
        return issuerId;
    }

    /**
     * D�finit la valeur de la propri�t� issuerId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssuerId(String value) {
        this.issuerId = value;
    }

    /**
     * Obtient la valeur de la propri�t� issuerTrackingId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssuerTrackingId() {
        return issuerTrackingId;
    }

    /**
     * D�finit la valeur de la propri�t� issuerTrackingId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssuerTrackingId(String value) {
        this.issuerTrackingId = value;
    }

    /**
     * Obtient la valeur de la propri�t� reason.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReason() {
        return reason;
    }

    /**
     * D�finit la valeur de la propri�t� reason.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReason(String value) {
        this.reason = value;
    }

    /**
     * Obtient la valeur de la propri�t� activationDateTime.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getActivationDateTime() {
        return activationDateTime;
    }

    /**
     * D�finit la valeur de la propri�t� activationDateTime.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setActivationDateTime(XMLGregorianCalendar value) {
        this.activationDateTime = value;
    }

}
