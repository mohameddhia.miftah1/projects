package tn.talan.pfe.config;

import javax.jms.ConnectionFactory;
import javax.jms.JMSException;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.annotation.JmsListenerConfigurer;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerEndpointRegistrar;

//@Configuration
//@ComponentScan
//@EnableJms
public class JmsConfig implements JmsListenerConfigurer {
	public static final String DISCOVERY_QUEUE_NAME = "discoveryReport.queue";
	public static final String INSTALL_QUEUE_NAME = "installReport.queue";

	@Bean
	public ConnectionFactory connectionFactory() throws JMSException {
		ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://localhost:61616");
//		Connection connection = connectionFactory.createConnection();
//		connection.start();
		return connectionFactory;
	}

	@Bean
	public JmsListenerContainerFactory<?> jmsListenerContainerFactory() throws JMSException {
		DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
		factory.setConnectionFactory(connectionFactory());
		// core poll size=4 threads and max poll size 8 threads
//		factory.setConcurrency("4-8");
		return factory;
	}

	@Override
	public void configureJmsListeners(JmsListenerEndpointRegistrar registrar) {
		// TODO Auto-generated method stub

	}

}
