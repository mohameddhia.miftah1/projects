//
// Ce fichier a �t� g�n�r� par l'impl�mentation de r�f�rence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apport�e � ce fichier sera perdue lors de la recompilation du sch�ma source. 
// G�n�r� le : 2019.04.12 � 09:51:52 AM WAT 
//


package tn.talan.pfe.XmlDto.install;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour ElectricMeter complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="ElectricMeter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="aepCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="form" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="base" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="voltageRating" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="currentRating" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="frequencyRating" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numPhases" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numWires" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="kh" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="registerRatio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ElectricMeter", propOrder = {
    "aepCode",
    "form",
    "base",
    "voltageRating",
    "currentRating",
    "frequencyRating",
    "numPhases",
    "numWires",
    "kh",
    "registerRatio"
})
public class ElectricMeter {

    protected String aepCode;
    protected String form;
    protected String base;
    protected String voltageRating;
    protected String currentRating;
    protected String frequencyRating;
    protected String numPhases;
    protected String numWires;
    protected String kh;
    protected String registerRatio;

    /**
     * Obtient la valeur de la propri�t� aepCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAepCode() {
        return aepCode;
    }

    /**
     * D�finit la valeur de la propri�t� aepCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAepCode(String value) {
        this.aepCode = value;
    }

    /**
     * Obtient la valeur de la propri�t� form.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForm() {
        return form;
    }

    /**
     * D�finit la valeur de la propri�t� form.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForm(String value) {
        this.form = value;
    }

    /**
     * Obtient la valeur de la propri�t� base.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBase() {
        return base;
    }

    /**
     * D�finit la valeur de la propri�t� base.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBase(String value) {
        this.base = value;
    }

    /**
     * Obtient la valeur de la propri�t� voltageRating.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoltageRating() {
        return voltageRating;
    }

    /**
     * D�finit la valeur de la propri�t� voltageRating.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoltageRating(String value) {
        this.voltageRating = value;
    }

    /**
     * Obtient la valeur de la propri�t� currentRating.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrentRating() {
        return currentRating;
    }

    /**
     * D�finit la valeur de la propri�t� currentRating.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrentRating(String value) {
        this.currentRating = value;
    }

    /**
     * Obtient la valeur de la propri�t� frequencyRating.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFrequencyRating() {
        return frequencyRating;
    }

    /**
     * D�finit la valeur de la propri�t� frequencyRating.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFrequencyRating(String value) {
        this.frequencyRating = value;
    }

    /**
     * Obtient la valeur de la propri�t� numPhases.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumPhases() {
        return numPhases;
    }

    /**
     * D�finit la valeur de la propri�t� numPhases.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumPhases(String value) {
        this.numPhases = value;
    }

    /**
     * Obtient la valeur de la propri�t� numWires.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumWires() {
        return numWires;
    }

    /**
     * D�finit la valeur de la propri�t� numWires.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumWires(String value) {
        this.numWires = value;
    }

    /**
     * Obtient la valeur de la propri�t� kh.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKh() {
        return kh;
    }

    /**
     * D�finit la valeur de la propri�t� kh.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKh(String value) {
        this.kh = value;
    }

    /**
     * Obtient la valeur de la propri�t� registerRatio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegisterRatio() {
        return registerRatio;
    }

    /**
     * D�finit la valeur de la propri�t� registerRatio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegisterRatio(String value) {
        this.registerRatio = value;
    }

}
