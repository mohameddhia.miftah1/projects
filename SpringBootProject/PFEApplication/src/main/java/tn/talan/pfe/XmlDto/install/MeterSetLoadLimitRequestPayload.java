//
// Ce fichier a �t� g�n�r� par l'impl�mentation de r�f�rence JavaTM Architecture for XML Binding (JAXB), v2.2.8-b130911.1802 
// Voir <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Toute modification apport�e � ce fichier sera perdue lors de la recompilation du sch�ma source. 
// G�n�r� le : 2019.04.12 � 09:51:52 AM WAT 
//


package tn.talan.pfe.XmlDto.install;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java pour MeterSetLoadLimitRequestPayload complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="MeterSetLoadLimitRequestPayload">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="meterAsset" type="{http://www.emeter.com/energyip/amiinterface}MeterAsset"/>
 *         &lt;element name="requestPriority" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="startDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="emergencyProfileStartTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="emergencyProfileDuration" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="loadLimitProfile" type="{http://www.emeter.com/energyip/amiinterface}ConfigurationProfile" minOccurs="0"/>
 *         &lt;element name="issuerId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="issuerTrackingId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="reason" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MeterSetLoadLimitRequestPayload", propOrder = {
    "meterAsset",
    "requestPriority",
    "startDateTime",
    "emergencyProfileStartTime",
    "emergencyProfileDuration",
    "loadLimitProfile",
    "issuerId",
    "issuerTrackingId",
    "reason"
})
public class MeterSetLoadLimitRequestPayload {

    @XmlElement(required = true)
    protected MeterAsset meterAsset;
    protected String requestPriority;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar startDateTime;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar emergencyProfileStartTime;
    @XmlElement(required = true)
    protected BigInteger emergencyProfileDuration;
    protected ConfigurationProfile loadLimitProfile;
    @XmlElement(required = true)
    protected String issuerId;
    @XmlElement(required = true)
    protected String issuerTrackingId;
    @XmlElement(required = true)
    protected String reason;

    /**
     * Obtient la valeur de la propri�t� meterAsset.
     * 
     * @return
     *     possible object is
     *     {@link MeterAsset }
     *     
     */
    public MeterAsset getMeterAsset() {
        return meterAsset;
    }

    /**
     * D�finit la valeur de la propri�t� meterAsset.
     * 
     * @param value
     *     allowed object is
     *     {@link MeterAsset }
     *     
     */
    public void setMeterAsset(MeterAsset value) {
        this.meterAsset = value;
    }

    /**
     * Obtient la valeur de la propri�t� requestPriority.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestPriority() {
        return requestPriority;
    }

    /**
     * D�finit la valeur de la propri�t� requestPriority.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestPriority(String value) {
        this.requestPriority = value;
    }

    /**
     * Obtient la valeur de la propri�t� startDateTime.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartDateTime() {
        return startDateTime;
    }

    /**
     * D�finit la valeur de la propri�t� startDateTime.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartDateTime(XMLGregorianCalendar value) {
        this.startDateTime = value;
    }

    /**
     * Obtient la valeur de la propri�t� emergencyProfileStartTime.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEmergencyProfileStartTime() {
        return emergencyProfileStartTime;
    }

    /**
     * D�finit la valeur de la propri�t� emergencyProfileStartTime.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEmergencyProfileStartTime(XMLGregorianCalendar value) {
        this.emergencyProfileStartTime = value;
    }

    /**
     * Obtient la valeur de la propri�t� emergencyProfileDuration.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getEmergencyProfileDuration() {
        return emergencyProfileDuration;
    }

    /**
     * D�finit la valeur de la propri�t� emergencyProfileDuration.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setEmergencyProfileDuration(BigInteger value) {
        this.emergencyProfileDuration = value;
    }

    /**
     * Obtient la valeur de la propri�t� loadLimitProfile.
     * 
     * @return
     *     possible object is
     *     {@link ConfigurationProfile }
     *     
     */
    public ConfigurationProfile getLoadLimitProfile() {
        return loadLimitProfile;
    }

    /**
     * D�finit la valeur de la propri�t� loadLimitProfile.
     * 
     * @param value
     *     allowed object is
     *     {@link ConfigurationProfile }
     *     
     */
    public void setLoadLimitProfile(ConfigurationProfile value) {
        this.loadLimitProfile = value;
    }

    /**
     * Obtient la valeur de la propri�t� issuerId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssuerId() {
        return issuerId;
    }

    /**
     * D�finit la valeur de la propri�t� issuerId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssuerId(String value) {
        this.issuerId = value;
    }

    /**
     * Obtient la valeur de la propri�t� issuerTrackingId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssuerTrackingId() {
        return issuerTrackingId;
    }

    /**
     * D�finit la valeur de la propri�t� issuerTrackingId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssuerTrackingId(String value) {
        this.issuerTrackingId = value;
    }

    /**
     * Obtient la valeur de la propri�t� reason.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReason() {
        return reason;
    }

    /**
     * D�finit la valeur de la propri�t� reason.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReason(String value) {
        this.reason = value;
    }

}
