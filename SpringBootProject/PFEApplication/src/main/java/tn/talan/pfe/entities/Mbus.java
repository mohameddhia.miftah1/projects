package tn.talan.pfe.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "mbus", schema = "public")
public class Mbus implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "identification_number", unique = true, nullable = false, length = 8)
	private String identificationNumber;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "meter_serial_number", nullable = false)
	private Meter meter;

	@Column(name = "manufacturer_identification", length = 3)
	private String manufacturerIdentification;

	@Column(name = "version_index", length = 5)
	private String versionIndex;

	@Column(name = "version_radio", length = 5)
	private String versionRadio;

	@Column(name = "device_type_identification", length = 2)
	private String deviceTypeIdentification;

	@Column(name = "device_pin_code", length = 4)
	private String devicePinCode;
}