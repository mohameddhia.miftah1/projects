package tn.talan.pfe.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import tn.talan.pfe.entities.Meter;

public interface MeterRepository extends JpaRepository<Meter, String> {
}