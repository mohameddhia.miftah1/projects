package tn.enis.SpringProject.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tn.enis.SpringProject.entities.Consultation;
import tn.enis.SpringProject.service.ConsultationService;

@RestController
@RequestMapping("/cabinet")
@CrossOrigin("*")
public class ConsultationController {

	@Autowired
	ConsultationService consultantService;

	@GetMapping("/getOneConsultation/{id}")
	public Consultation getOneConsultation(@PathVariable("id") Long id) {
		return consultantService.getOneConsultation(id);
	}

	@GetMapping("/getAllConsultation")
	public List<Consultation> getAllConsultation() {
		return consultantService.getAllConsultation();
	}

	@PostMapping(value = "/addConsultation")
	public void AddConsultation(@RequestBody Consultation consultation) {
		consultantService.addConsultation(consultation);
	}

	@PutMapping(value = "/updateConsultation/{id}")
	public void UpdateConsultation(@PathVariable("id") Long id, @RequestBody Consultation consultation) {
		consultantService.updateConsultation(id, consultation);
	}

	@DeleteMapping("/deleteConsultation/{id}")
	public void deleteConsultation(@PathVariable Long id) {
		consultantService.deleteConsultation(id);
	}
	
	@GetMapping("/sortConsultationByDate")
	public List<Consultation> sortConsultationByDate() {
		return consultantService.sortByDate();
	}
}
