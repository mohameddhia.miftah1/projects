package tn.enis.SpringProject.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Patient implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Long Id;
	private String nom;
	private String prenom;
	@Temporal(TemporalType.DATE)
	private Date dateNaissance;
	@Lob
	private byte[] photo;
	private String email;
	private String tel;

	@ManyToOne(fetch = FetchType.EAGER)
	private MedecinGeneraliste generaliste;

	@ManyToMany(mappedBy = "patients")
	private Collection<MedecinSpecialiste> specialistes;

	@JsonIgnore
	@OneToMany(mappedBy = "patient", cascade = CascadeType.ALL)
	private Collection<Consultation> consultations;

	@OneToOne(mappedBy = "patientUser")
	private User user;

	public Patient() {
		super();
	}

	public Patient(String nom, String prenom, Date dateNaissance, byte[] photo, String email, String tel) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.dateNaissance = dateNaissance;
		this.photo = photo;
		this.email = email;
		this.tel = tel;
	}

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public Date getDateNaissance() {
		return dateNaissance;
	}

	public void setDateNaissance(Date dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	public byte[] getPhoto() {
		return photo;
	}

	public void setPhoto(byte[] photo) {
		this.photo = photo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public MedecinGeneraliste getGeneraliste() {
		return generaliste;
	}

	public void setGeneraliste(MedecinGeneraliste generaliste) {
		this.generaliste = generaliste;
	}

	public Collection<MedecinSpecialiste> getSpecialistes() {
		return specialistes;
	}

	public void setSpecialistes(Collection<MedecinSpecialiste> specialistes) {
		this.specialistes = specialistes;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
