package tn.com.Compte;

import java.util.Date;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;

import tn.com.Compte.entities.Compte;
import tn.com.Compte.enums.TypeCompte;
import tn.com.Compte.repositories.ICompteRepository;

@SpringBootApplication
public class CompteApplication {

	public static void main(String[] args) {
		SpringApplication.run(CompteApplication.class, args);
	}

//	@Override
//	public void run(String... args) throws Exception {
//		// TODO Auto-generated method stub
//		
//	}

	@Bean
	CommandLineRunner start(ICompteRepository compteRepository, RepositoryRestConfiguration restConfiguration) {
		return args -> {
			restConfiguration.exposeIdsFor(Compte.class);
			compteRepository.save(new Compte(null, 10000, new Date(), TypeCompte.COURANT));
			compteRepository.save(new Compte(null, 20000, new Date(), TypeCompte.EPARGNE));
			compteRepository.save(new Compte(null, 30000, new Date(), TypeCompte.COURANT));

//			compteRepository.findAll().forEach(cp -> {
//				System.out.println("compte type = " + cp.getType());
//				System.out.println("compte solde = " + cp.getSolde());
//			});
		};
	}

}
