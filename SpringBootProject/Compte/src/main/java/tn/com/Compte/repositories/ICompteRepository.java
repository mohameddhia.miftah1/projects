package tn.com.Compte.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import tn.com.Compte.entities.Compte;
import tn.com.Compte.enums.TypeCompte;

@RepositoryRestResource
public interface ICompteRepository extends JpaRepository<Compte, Long> {
	
	@RestResource(path = "/byType")
	List<Compte> findByType(@Param(value="type") TypeCompte typeCompte);

}
