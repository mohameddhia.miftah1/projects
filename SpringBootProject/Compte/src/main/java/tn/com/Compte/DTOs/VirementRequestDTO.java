package tn.com.Compte.DTOs;

import lombok.Data;

@Data
public class VirementRequestDTO {

	private Long codeSource;
	private Long codeDestination;
	private double amount;
}
